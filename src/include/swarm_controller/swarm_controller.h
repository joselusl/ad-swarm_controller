
#ifndef _SWARM_CONTROLLER_H_
#define _SWARM_CONTROLLER_H_


// string
#include <string>

// thread
#include <thread>

// ros
#include <ros/ros.h>

//
#include <std_srvs/Empty.h>

//
#include <geometry_msgs/PoseStamped.h>

//
#include <nav_msgs/Path.h>
#include <nav_msgs/OccupancyGrid.h>

// tf
#include <tf/transform_datatypes.h>
#include <tf/transform_listener.h>
//#include <tf/transform_broadcaster.h>


// exploration_msgs
#include <exploration_msgs/ExplorationMission.h>



// robot interface
#include "swarm_controller/robot_interface.h"




////////////////////////////
/// \brief The SwarmController class
/////////////////////////////
class SwarmController
{
public:
    SwarmController(int argc,char **argv);
    ~SwarmController();

    // Node handler
protected:
    ros::NodeHandle* nh_;

    // Tf
protected:
    tf::TransformListener* tf_transform_listener_;
    // tf::TransformBroadcaster* tf_transform_broadcaster_;


    // started flag
protected:
    bool flag_started_;

    // start
public:
    int start();
protected:
    ros::ServiceServer start_srv_;
    bool startSrvCallback(std_srvs::Empty::Request& req, std_srvs::Empty::Response& res);

    // stop
public:
    int stop();
protected:
    ros::ServiceServer stop_srv_;
    bool stopSrvCallback(std_srvs::Empty::Request& req, std_srvs::Empty::Response& res);

    // isStarted
public:
    bool isStarted() const;

protected:
    void init();

protected:
    void create();
    void destroy();

protected:
    void readParameters();

protected:
    void startThreads();
    void stopThreads();

public:
    void open();
    void close();

public:
    void run();
    void auto_run();

protected:
    std::thread* spinner_thread_;
protected:
    void spinnerThread();


protected:
    std::list<RobotInterface*> list_robot_interfaces_;


};








#endif
