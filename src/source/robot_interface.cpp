
#include "swarm_controller/robot_interface.h"



RobotInterface::RobotInterface(ros::NodeHandle* nh, tf::TransformListener* tf_transform_listener)
{
    nh_=nh;
    tf_transform_listener_=tf_transform_listener;

    // Default values
    id_=0;

    return;
}

RobotInterface::~RobotInterface()
{
    return;
}

void RobotInterface::setId(int id)
{
    id_=id;
    return;
}

int RobotInterface::getId() const
{
    return id_;
}

int RobotInterface::start()
{
    std_srvs::Empty start_srv_msg;
    if(!start_srv_.call(start_srv_msg))
    {
        return -1;
    }
    return 0;
}

int RobotInterface::stop()
{
    std_srvs::Empty stop_srv_msg;
    if(!stop_srv_.call(stop_srv_msg))
    {
        return -1;
    }
    return 0;
}

int RobotInterface::explore(exploration_msgs::ExplorationMission exploration_srv_msg)
{
    // Call
    if(!exploration_srv_.call(exploration_srv_msg))
    {
        return -1;
    }

    return 0;
}

int RobotInterface::open()
{
    // start service
    start_srv_=nh_->serviceClient<std_srvs::Empty>("robot_"+std::to_string(id_)+"/robot_controller/start");

    // stop service
    stop_srv_=nh_->serviceClient<std_srvs::Empty>("robot_"+std::to_string(id_)+"/robot_controller/stop");

    // exploration service
    exploration_srv_=nh_->serviceClient<exploration_msgs::ExplorationMission>("robot_"+std::to_string(id_)+"/robot_controller/explore_map2d");


    return 0;
}

