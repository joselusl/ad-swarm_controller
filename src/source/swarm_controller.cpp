
#include "swarm_controller/swarm_controller.h"


SwarmController::SwarmController(int argc,char **argv)
{
    //Ros Init
    ros::init(argc, argv, ros::this_node::getName());

    // Node handle
    nh_=new ros::NodeHandle();

    // Init
    init();

    // Create
    create();

    // Read parameters
    readParameters();

    // start Threads
    startThreads();

    return;
}

SwarmController::~SwarmController()
{
    // close
    try
    {
        close();
    }
    catch(...)
    {
        // Do nothing
        ROS_INFO("error close()");
    }

    // be tidy: destroy
    try
    {
        destroy();
    }
    catch(...)
    {
        // Do nothing
        ROS_INFO("error destroy()");
    }

    // stop threads
    stopThreads();

    return;
}

void SwarmController::init()
{
    flag_started_=false;
}

void SwarmController::create()
{
    // tf listener
    tf_transform_listener_=new tf::TransformListener;
    // tf broadcaster
    //tf_transform_broadcaster_=new tf::TransformBroadcaster;

    return;
}

void SwarmController::destroy()
{
    // tf listener
    delete tf_transform_listener_;
    // tf broadcaster
    //delete tf_transform_broadcaster_;

    return;
}

void SwarmController::readParameters()
{

}

void SwarmController::startThreads()
{
    spinner_thread_=new std::thread(&SwarmController::spinnerThread, this);

    return;
}

void SwarmController::stopThreads()
{
    // Do nothing
}

void SwarmController::open()
{
    // Start service
    start_srv_ = nh_->advertiseService(ros::this_node::getName()+"/start", &SwarmController::startSrvCallback, this);

    // Stop service
    stop_srv_ = nh_->advertiseService(ros::this_node::getName()+"/stop", &SwarmController::stopSrvCallback, this);



    return;
}

void SwarmController::close()
{

    return;
}

void SwarmController::run()
{
    // Do nothing

    // Waits until the other thread dies
    spinner_thread_->join();

    return;
}

void SwarmController::auto_run()
{
    // sleep a while
    ros::Duration(5).sleep();

    // start swarm controller
    start();

    ROS_INFO("Preparing Swarm");

    // Create list of robots
    // Robot 0
    {
        ROS_INFO("Preparing robot 0");
        RobotInterface* robot_interface=new RobotInterface(nh_, tf_transform_listener_);
        robot_interface->setId(0);
        robot_interface->open();

        // start robot
        if(robot_interface->start())
            ROS_ERROR("Error starting robot 0");

        // Set exploration task to robot
        exploration_msgs::ExplorationMission exploration_srv_msg;
        // Header
        exploration_srv_msg.request.header.frame_id="/world";
        exploration_srv_msg.request.header.stamp=ros::Time::now();
        // Final point
        exploration_srv_msg.request.final_point.header.frame_id="/world";
        exploration_srv_msg.request.final_point.header.stamp=ros::Time::now();
        exploration_srv_msg.request.final_point.pose.position.x=55;
        exploration_srv_msg.request.final_point.pose.position.y=5;
        exploration_srv_msg.request.final_point.pose.position.z=0;
        exploration_srv_msg.request.final_point.pose.orientation.w=1;
        exploration_srv_msg.request.final_point.pose.orientation.x=0;
        exploration_srv_msg.request.final_point.pose.orientation.y=0;
        exploration_srv_msg.request.final_point.pose.orientation.z=0;
        // Map Mask
        int width=600;
        int height=600;
        exploration_srv_msg.request.map_mask.header.frame_id="/world";
        exploration_srv_msg.request.map_mask.header.stamp=ros::Time::now();
        exploration_srv_msg.request.map_mask.info.height=height;
        exploration_srv_msg.request.map_mask.info.width=width;
        exploration_srv_msg.request.map_mask.info.resolution=0.1;
        exploration_srv_msg.request.map_mask.data.resize(height * width);
        int val_i=0;
        for(int i=0; i<width; i++)
        {
            for(int j=0; j<height; j++)
            {
                if(j<=200)
                {
                    exploration_srv_msg.request.map_mask.data[val_i]=1;
                }
                else
                {
                    exploration_srv_msg.request.map_mask.data[val_i]=0;
                }
                val_i++;
            }
        }
        // Explore
        if(robot_interface->explore(exploration_srv_msg))
            ROS_ERROR("Error sending exploration mission to robot 0");


        list_robot_interfaces_.push_back(robot_interface);
    }

    // Robot 1
    {
        ROS_INFO("Preparing robot 1");
        RobotInterface* robot_interface=new RobotInterface(nh_, tf_transform_listener_);
        robot_interface->setId(1);
        robot_interface->open();

        // start robot
        robot_interface->start();

        // Set exploration task
        exploration_msgs::ExplorationMission exploration_srv_msg;
        // Header
        exploration_srv_msg.request.header.frame_id="/world";
        exploration_srv_msg.request.header.stamp=ros::Time::now();
        // Final point
        exploration_srv_msg.request.final_point.header.frame_id="/world";
        exploration_srv_msg.request.final_point.header.stamp=ros::Time::now();
        exploration_srv_msg.request.final_point.pose.position.x=55;
        exploration_srv_msg.request.final_point.pose.position.y=30;
        exploration_srv_msg.request.final_point.pose.position.z=0;
        exploration_srv_msg.request.final_point.pose.orientation.w=1;
        exploration_srv_msg.request.final_point.pose.orientation.x=0;
        exploration_srv_msg.request.final_point.pose.orientation.y=0;
        exploration_srv_msg.request.final_point.pose.orientation.z=0;
        // Map Mask
        int width=600;
        int height=600;
        exploration_srv_msg.request.map_mask.header.frame_id="/world";
        exploration_srv_msg.request.map_mask.header.stamp=ros::Time::now();
        exploration_srv_msg.request.map_mask.info.height=height;
        exploration_srv_msg.request.map_mask.info.width=width;
        exploration_srv_msg.request.map_mask.info.resolution=0.1;
        exploration_srv_msg.request.map_mask.data.resize(height * width);
        int val_i=0;
        for(int i=0; i<width; i++)
        {
            for(int j=0; j<height; j++)
            {
                if(j<=400 && j>=200)
                {
                    exploration_srv_msg.request.map_mask.data[val_i]=1;
                }
                else
                {
                    exploration_srv_msg.request.map_mask.data[val_i]=0;
                }
                val_i++;
            }
        }
        // Explore
        if(robot_interface->explore(exploration_srv_msg))
            ROS_ERROR("Error sending exploration mission to robot 1");

        list_robot_interfaces_.push_back(robot_interface);
    }

    // Robot 2
    {
        ROS_INFO("Preparing robot 2");
        RobotInterface* robot_interface=new RobotInterface(nh_, tf_transform_listener_);
        robot_interface->setId(2);
        robot_interface->open();

        // start robot
        robot_interface->start();

        // Set exploration task
        exploration_msgs::ExplorationMission exploration_srv_msg;
        // Header
        exploration_srv_msg.request.header.frame_id="/world";
        exploration_srv_msg.request.header.stamp=ros::Time::now();
        // Final point
        exploration_srv_msg.request.final_point.header.frame_id="/world";
        exploration_srv_msg.request.final_point.header.stamp=ros::Time::now();
        exploration_srv_msg.request.final_point.pose.position.x=55;
        exploration_srv_msg.request.final_point.pose.position.y=55;
        exploration_srv_msg.request.final_point.pose.position.z=0;
        exploration_srv_msg.request.final_point.pose.orientation.w=1;
        exploration_srv_msg.request.final_point.pose.orientation.x=0;
        exploration_srv_msg.request.final_point.pose.orientation.y=0;
        exploration_srv_msg.request.final_point.pose.orientation.z=0;
        // Map Mask
        int width=600;
        int height=600;
        exploration_srv_msg.request.map_mask.header.frame_id="/world";
        exploration_srv_msg.request.map_mask.header.stamp=ros::Time::now();
        exploration_srv_msg.request.map_mask.info.height=height;
        exploration_srv_msg.request.map_mask.info.width=width;
        exploration_srv_msg.request.map_mask.info.resolution=0.1;
        exploration_srv_msg.request.map_mask.data.resize(height * width);
        int val_i=0;
        for(int i=0; i<width; i++)
        {
            for(int j=0; j<height; j++)
            {
                if(j>=400)
                {
                    exploration_srv_msg.request.map_mask.data[val_i]=1;
                }
                else
                {
                    exploration_srv_msg.request.map_mask.data[val_i]=0;
                }
                val_i++;
            }
        }
        // Explore
        if(robot_interface->explore(exploration_srv_msg))
            ROS_ERROR("Error sending exploration mission to robot 0");

        list_robot_interfaces_.push_back(robot_interface);
    }





    // Waits until the other thread dies
    spinner_thread_->join();

    // stop swarm controller
    stop();

    return;
}


bool SwarmController::startSrvCallback(std_srvs::Empty::Request& req, std_srvs::Empty::Response& res)
{
    start();
    return false;
}

bool SwarmController::stopSrvCallback(std_srvs::Empty::Request& req, std_srvs::Empty::Response& res)
{
    stop();
    return false;
}

int SwarmController::start()
{
    if(!isStarted())
        flag_started_=true;
    return 0;
}

int SwarmController::stop()
{
    if(isStarted())
        flag_started_=false;
    return 0;
}

bool SwarmController::isStarted() const
{
    return flag_started_;
}

void SwarmController::spinnerThread()
{
    ros::spin();
}
