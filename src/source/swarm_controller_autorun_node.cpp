

//I/O stream
//std::cout
#include <iostream>

//
#include "swarm_controller/swarm_controller.h"


int main(int argc,char **argv)
{
    SwarmController swarm_controller(argc, argv);
    ROS_INFO("Starting %s",ros::this_node::getName().c_str());
    // open
    swarm_controller.open();
    // run
    swarm_controller.auto_run();

    return 0;
}

