
#ifndef _ROBOT_INTERFACE_H_
#define _ROBOT_INTERFACE_H_


// string
#include <string>

// thread
#include <thread>

// ros
#include <ros/ros.h>

//
#include <std_srvs/Empty.h>

//
#include <geometry_msgs/PoseStamped.h>

//
#include <nav_msgs/Path.h>
#include <nav_msgs/OccupancyGrid.h>

// tf
#include <tf/transform_datatypes.h>
#include <tf/transform_listener.h>
//#include <tf/transform_broadcaster.h>


// exploration_msgs
#include <exploration_msgs/ExplorationMission.h>




//////////////////////////////////////
/// \brief The RobotInterface class
/////////////////////////////////////
class RobotInterface
{
public:
    RobotInterface(ros::NodeHandle* nh, tf::TransformListener* tf_transform_listener);
    ~RobotInterface();

    // Node handler
protected:
    ros::NodeHandle* nh_;

    // Tf
protected:
    tf::TransformListener* tf_transform_listener_;

    // Robot id
public:
    int id_;
public:
    void setId(int id);
    int getId() const;

    // Start service
protected:
    ros::ServiceClient start_srv_;
public:
    int start();

    // Stop service
protected:
    ros::ServiceClient stop_srv_;
public:
    int stop();

    // Get Pose
    // TODO

    // Exploration service (and action would be better)
protected:
    ros::ServiceClient exploration_srv_;
public:
    int explore(exploration_msgs::ExplorationMission exploration_srv_msg);

    // open
public:
    int open();

};







#endif
